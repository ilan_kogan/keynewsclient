import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import imgNews from './newsImage.png';
import { selectArticle } from '../../Containers/NewsFeed/actions.js'

const ArticleCard = ({ link, author, title, description, imageLink, publishedAt, articleSelect, id, selectedArticle }) => {
    const [display, setDisplay] = useState('none');


    useEffect(() => {
        if (selectedArticle === null || selectedArticle !== id)
            setDisplay('none');
        else
            setDisplay('block');
    }, [selectedArticle]);

    const handleClick = (e) => {
        e.stopPropagation();
        if (e.target.children.length > 0) return;
        if (display === 'none')
            articleSelect(id);
        else
            articleSelect(null);
    };
    return (
        <div style={{ width: "100%", margin: "0.5rem" }}>
            <div className="news-card" onClick={handleClick}>
                <a className="news-card__card-link"></a>
                <img src={imageLink === null ? imgNews : imageLink} alt={`Image: ${title}`} className="news-card__image" />
                <div className="news-card__text-wrapper">
                    <h2 className="news-card__title">{title}</h2>
                    {author === null ? <div className="news-card__post-date">{`Published at ${publishedAt}`}</div> :
                        <div className="news-card__post-date">{`${author.split(' ')[0] === 'By' ? '' : 'By '}${author}, published at ${publishedAt}`}</div>}
                    <div className="news-card__details-wrapper">
                        <p className="news-card__excerpt">{description=== 'null'? '': description}</p>
                        <a href={link} target="_blank" className="news-card__read-more" onClick={handleClick}>Go to site<i className="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div>
                {/* doing it like this and not by display:none on style attribute to prevent from running videos on the background*/}
                {display === 'none' ? '' : (<iframe src={link}
                    width="100%"
                    height="450px"
                    className="cardIframe"
                    title={title}>
                </iframe>)}

            </div>
        </div >
    );
};


const mapDispatchToProps = dispatch => {
    return {
        articleSelect: (id) => dispatch(selectArticle(id))
    }
};

const mapStateToProps = state => {
    return {
        selectedArticle: state.getIn(['feedContainer', 'selectedArticle'])
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ArticleCard);
