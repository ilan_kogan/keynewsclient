import React, { useState } from 'react';
import { connect } from 'react-redux';
import styles from '../Keyword/styles.module.css';
import { addKeywordModeOff, addKeyword } from '../../Containers/Articles/actions';

const Keyword = ({ id, value, language, addKeyword, cancelAddword }) => {

    const mapLanguageValueToDescription = {
        'he': 'Hebrew',
        'en': 'English'
    };
    const [keyLanguage, setKeyLanguage] = useState('en');
    const [keyValue, setKeyValue] = useState('');

    const handleKeyValueChange = (event) => {
        setKeyValue(event.target.value)
    };

    const handleCancleClick = () => {
        cancelAddword();
    };

    const handleAddClick = () => {
        if (keyValue.trim().length !== 0) {
            addKeyword(keyValue.trim(), keyLanguage);
        }
    };
    const handleEnter = (event) => {
        if (event.keyCode === 13 && keyValue.trim().length !== 0)
            addKeyword(keyValue.trim(), keyLanguage);
    }
    const handleSelect = (event) => {
        setKeyLanguage(event.target.value);
    };
    return (
        <div className={styles.wrapper}>
            <div className={styles.editInputs}>
                <React.Fragment><input style={{ maxWidth: `${100}px` }} className=" keywordInput" type={'text'} defaultValue={keyValue} onChange={handleKeyValueChange} onKeyUp={handleEnter} />
                    <select className={styles.select} onChange={handleSelect} defaultValue={keyLanguage}>
                        <option value={'en'}>English</option>
                        <option value="he">Hebrew</option>
                    </select>
                </React.Fragment>
            </div>
            <div className={styles.buttons}>
                <React.Fragment>
                    <a className="label theme-bg-red text-white f-12 roundRadius" onClick={handleCancleClick}>Cancel</a>
                    <a className="label theme-bg text-white f-12 roundRadius" onClick={handleAddClick}>Add</a>
                </React.Fragment>
            </div>
        </div>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        addKeyword: (value, language) => dispatch(addKeyword(value, language)),
        cancelAddword: () => dispatch(addKeywordModeOff())
    }
};

export default connect(null, mapDispatchToProps)(Keyword);
