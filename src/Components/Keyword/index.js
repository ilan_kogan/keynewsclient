import React, { useState } from 'react';
import { connect } from 'react-redux';
import styles from './styles.module.css';
import { fetchKeywords, editKeyword, deleteKeyword as deleteKeywordAction } from '../../Containers/Articles/actions';
import deleteIcon from '../../assets/images/keywords/delete.svg';

const Keyword = ({ id, value, language, confirmEditKeyword, isDeleteMode, deleteKeyword }) => {

    const mapLanguageValueToDescription = {
        'he': 'Hebrew',
        'en': 'English'
    };
    const [keyLanguage, setKeyLanguage] = useState(language);
    const [editKeyLanguage, setEditKeyLanguage] = useState(language);
    const [keyValue, setKeyValue] = useState(value);
    const [editKeyValue, setEditKeyValue] = useState(value);
    const [inEditMode, setInEditMode] = useState(false);

    const handleEditKeyValueChange = (event) => {
        setEditKeyValue(event.target.value)
    };

    const handleEditClick = () => {
        setInEditMode(true);
    };

    const handleCancleClick = () => {
        setInEditMode(false);
        setEditKeyValue(keyValue);
        setEditKeyLanguage(keyLanguage)
    };

    const handleConfirmClick = () => {
        if ((editKeyValue !== keyValue || editKeyLanguage !== keyLanguage) && editKeyValue.trim().length !== 0) {
            confirmEditKeyword(id, editKeyValue.trim(), editKeyLanguage);
        }
    };

    const handleSelect = (event) => {
        setEditKeyLanguage(event.target.value);
    };

    const handleEnter = (event) => {
        if (event.keyCode === 13)
            handleConfirmClick()
    }
    return (
        <div className={styles.wrapper}>
            <div className={styles.editInputs}>
                {inEditMode ? <React.Fragment><input style={{ maxWidth: `${100}px` }} className=" keywordInput" type={'text'} defaultValue={editKeyValue} onChange={handleEditKeyValueChange} onKeyUp={handleEnter} />
                    <select className={styles.select} onChange={handleSelect} defaultValue={keyLanguage}>
                        <option value={'en'}>English</option>
                        <option value="he">Hebrew</option>
                    </select>
                </React.Fragment> : <React.Fragment>
                        <h6 className="mb-1">{`Value: ${keyValue}, Articles language: ${mapLanguageValueToDescription[keyLanguage]}`}</h6>
                    </React.Fragment>}

            </div>

            <div className={styles.buttons}>
                {inEditMode ?
                    <React.Fragment>
                        <a className="label theme-bg-red text-white f-12 roundRadius" onClick={handleCancleClick}>Cancel</a>
                        <a className="label theme-bg text-white f-12 roundRadius" onClick={handleConfirmClick}>Confirm</a>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        {/*<h6 className="mb-1">{language}</h6>*/}
                        {isDeleteMode ? <img className={styles.delete} src={deleteIcon} onClick={() => deleteKeyword(id)} /> : <a className="label theme-bg2 text-white f-12 roundRadius" onClick={handleEditClick}>Edit</a>}
                    </React.Fragment>
                }
            </div>
        </div>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        confirmEditKeyword: (keywordId, value, language) => dispatch(editKeyword(keywordId, value, language)),
        deleteKeyword: (keywordId) => dispatch(deleteKeywordAction(keywordId))
    }
};

const mapStateToProps = state => {
    return {
        isDeleteMode: state.getIn(['articlesContainer', 'deleteMode']),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Keyword);
