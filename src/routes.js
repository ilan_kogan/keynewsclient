import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;
const DashboardDefault = React.lazy(() => import('./Containers/Articles'));
const FeedDefault = React.lazy(() => import('./Containers/NewsFeed'));
const routes = [
    { path: '/', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/news', exact: true, name: 'Default', component: FeedDefault },
];

export default routes;
