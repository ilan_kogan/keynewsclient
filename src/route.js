import React from 'react';

const register = React.lazy(() => import('./Containers/Auth/Register/index'));
const Login = React.lazy(() => import('./Containers/Auth/Login/index'));

const route = [
    { path: '/register', exact: true, name: 'register', component: register },
    { path: '/login', exact: true, name: 'loginIn', component: Login }
];

export default route;
