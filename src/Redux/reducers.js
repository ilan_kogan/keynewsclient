import { combineReducers } from "redux-immutable";
import { connectRouter } from 'connected-react-router/immutable'
import authContainer from '../Containers/Auth/reducer';
import articlesContainer from '../Containers/Articles/reducer';
import templateContainer from '../Containers/TemplateContainer/reducer';
import feedContainer from '../Containers/NewsFeed/reducer';
import { reducer as toastrReducer } from 'react-redux-toastr';

export default (history) => combineReducers({
    authContainer,
    articlesContainer,
    templateContainer,
    feedContainer,
    router: connectRouter(history),
    toastr: toastrReducer
});
