import LoginSaga from '../Containers/Auth/Login/saga';
import RegisterSaga from '../Containers/Auth/Register/saga';
import ArticlesSaga from '../Containers/Articles/saga';
import FeedSaga from '../Containers/NewsFeed/saga';

export default [
    LoginSaga,
    RegisterSaga,
    ArticlesSaga,
    FeedSaga
]
