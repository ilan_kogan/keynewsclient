import LoginSaga from '../Containers/Auth/sagas/login';
import RegisterSaga from '../Containers/Auth/sagas/actions';

export default [
    LoginSaga,
    RegisterSaga
]
