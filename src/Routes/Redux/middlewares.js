import createSagaMiddleware from 'redux-saga';
import {usernameMiddleware} from "../helpers/usernameMiddleware";
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

export default [usernameMiddleware, sagaMiddleware];

export function loadMiddlewares(dispatch){
    for (let saga of sagas){
        sagaMiddleware.run(saga, dispatch);
    }
}
