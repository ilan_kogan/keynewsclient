import {combineReducers} from "redux-immutable";
import { connectRouter } from 'connected-react-router/immutable'
import authContainer from '../Containers/Auth/reducer';

export default (history) => combineReducers({
    authContainer,
    router: connectRouter(history)
});
