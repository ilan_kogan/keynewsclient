import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";

const AuthRoute = props => {
    const { isLoggedIn } = props;
    if (isLoggedIn) return <Redirect to="/" />;
    else if (!isLoggedIn) return <Redirect to="/login" />;

    return <Route {...props} />;
};

const mapStateToProps = (state) => ({
    isLoggedIn: state.getIn(['authContainer', 'loggedIn'])
});

export default connect(mapStateToProps)(AuthRoute);
