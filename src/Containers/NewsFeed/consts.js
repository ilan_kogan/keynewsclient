export const FETCH_ARTICLES = 'FETCH_ARTICLES';
export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_FAIL = 'FETCH_ARTICLES_FAIL';
export const SELECT_ARTICLE = 'SELECT_ARTICLE';

export const LOGOUT = 'LOGOUT';
