import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Row, Col, Card, Table, Tabs, Tab } from 'react-bootstrap';
import { connect } from 'react-redux';
import Aux from "../../hoc/_Aux";
import ArticleCard from "../../Components/ArticleCard"
import '../../Components/ArticleCard/articleCardStyle.css'
import { fetchArticles } from './actions'

const Feed = ({ getArticles, articles, isArticlesLoading, errorFetchingArticles }) => {

    const [lastOffset, setLastOffset] = useState(0);

    useEffect(() => {
        getNextArticles()
    }, []);

    useEffect(() => {
        if (articles.toJS().length === lastOffset && lastOffset != 0)
            getNextArticles()
    }, [lastOffset]);

    const getNextArticles = () => {
        const id = articles[0] === undefined ? undefined : articles[0].Id;
        const limit = 5;
        const offset = articles.toJS().length;
        getArticles(limit, offset, id);
    }

    const debounce = (func, delay) => {
        let inDebounce
        return function () {
            const context = this;
            const args = arguments;
            clearTimeout(inDebounce);
            inDebounce = setTimeout(() => func.apply(context, args), delay);
        }
    }
    const handleScroll = () => {
        if (window.scrollY + window.innerHeight + 1 >= document.body.offsetHeight)
            setLastOffset(articles.toJS().length)
    }
    window.addEventListener('scroll', debounce(handleScroll, 100));
    return (
        <Aux className="feedContainer">
            <Row>
                <h1 id="header">Articles Received To Your Email</h1>
            </Row>
            <Row>
                <div class="content-wrapper">
                    {articles.toJS().map((article, index) => <ArticleCard key={index} id={article.id} link={article.link} author={article.author} title={article.title}
                        description={article.description} imageLink={article.imageLink} publishedAt={moment(article.publishedAt).format("DD/MM/YYYY")} />)}
                </div>
            </Row>
            <Row>
                <div style={{ margin: "auto" }}>
                    {isArticlesLoading ?
                        <h1 className="f-w-300 d-flex align-items-center m-b-0">Loading...</h1>
                        // <div class="progress">
                        //     <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">

                        //     </div>
                        // </div>
                        : errorFetchingArticles ?
                            <h1 className="f-w-300 d-flex align-items-center m-b-0">Error getting your news, please try to clear site data</h1>
                            : articles.toJS().length === 0 && <h2 className="f-w-300 d-flex align-items-center m-b-0">You did not receive articles to your emails yet</h2>}
                </div>
            </Row>
        </Aux>
    )
};

const mapDispatchToProps = dispatch => {
    return {
        getArticles: (limit, offset, id) => dispatch(fetchArticles(limit, offset, id))
    }
};

const mapStateToProps = state => {
    return {
        articles: state.getIn(['feedContainer', 'articles']),
        isArticlesLoading: state.getIn(['feedContainer', 'isArticlesLoading']),
        errorFetchingArticles: state.getIn(['feedContainer', 'errorFetchingArticles'])
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Feed);
