import { toastr } from 'react-redux-toastr'
import { takeEvery, call, put } from 'redux-saga/effects';
import {
    FETCH_ARTICLES
} from './consts';
import {
    fetchArticlesFail,
    fetchArticlesSuccess
} from './actions';
import {
    fetchArticles
} from '../../Api/userService/articles';



function* getArticles({ setting }) {
    try {
        const { limit, offset, id } = setting;
        const articles = yield call(fetchArticles, limit, offset, id);
        yield put(fetchArticlesSuccess(articles));
    }
    catch (error) {
        yield put(fetchArticlesFail());
        toastr.error('Server Error', "Can't get your news")
    }
}

export default function* defaultSaga() {
    yield takeEvery(FETCH_ARTICLES, getArticles);
}
