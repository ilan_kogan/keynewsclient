import {
    FETCH_ARTICLES,
    FETCH_ARTICLES_FAIL,
    FETCH_ARTICLES_SUCCESS,
    SELECT_ARTICLE
} from './consts';
export function fetchArticles(limit, offset, id) {
    return {
        type: FETCH_ARTICLES,
        setting: { limit, offset, id }
    }
}

export function fetchArticlesSuccess(articles) {
    return {
        type: FETCH_ARTICLES_SUCCESS,
        articles
    }
}

export function fetchArticlesFail() {
    return {
        type: FETCH_ARTICLES_FAIL
    }
}

export function selectArticle(id) {
    return {
        type: SELECT_ARTICLE,
        id
    }
}