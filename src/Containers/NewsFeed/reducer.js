import {
    FETCH_ARTICLES,
    FETCH_ARTICLES_FAIL,
    FETCH_ARTICLES_SUCCESS,
    SELECT_ARTICLE,
    LOGOUT
} from './consts';

import { fromJS } from "immutable";

const initialState = fromJS({
    articles: [],
    isArticlesLoading: false,
    errorFetchingArticles: false,
    selectedArticle: null
});

export default function ArticlesReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_ARTICLES:
            return state.set('isArticlesLoading', fromJS(true));
        case FETCH_ARTICLES_SUCCESS:
            return state.update('articles', articles => articles.push(...action.articles)).set('isArticlesLoading', fromJS(false)).set('errorFetchingArticles', fromJS(false));
        case FETCH_ARTICLES_FAIL:
            return state.set('isArticlesLoading', fromJS(false)).set('errorFetchingArticles', fromJS(true));
        case SELECT_ARTICLE:
            return state.set('selectedArticle', fromJS(action.id));
        case LOGOUT:
            return initialState;
        default:
            return state
    }
}
