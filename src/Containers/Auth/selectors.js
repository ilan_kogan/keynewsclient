import {createSelector} from 'reselect';
import {AUTH_CONTAINER,
        USER,
        USER_ID,
        LOGGEDIN,
} from "../../Redux/consts";

const getIsLoggedIn = state => state.getIn([AUTH_CONTAINER, LOGGEDIN]);
const getUser = state => state.getIn([AUTH_CONTAINER, USER]);
const getUserId = state => state.getIn([AUTH_CONTAINER, USER, USER_ID]);

export const loggedInSelector = createSelector(
    getIsLoggedIn,
    loggedIn => loggedIn
);

export const userSelector = createSelector(
    getUser,
    user => user
);

export const userIdSelector = createSelector(
    getUserId,
    id => id
)
