import {
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT,
    REGISTER_CHECK_EMAIL_SUCCESS,
    REGISTER_CHECK_USER_NAME_SUCCESS,
    FETCH_PROFILE_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    REGISTER_CHECK_VALIDATION
} from "./consts";

import { fromJS } from "immutable";

const user = JSON.parse(localStorage.getItem('user'));

const initialState = fromJS({
    loggedIn: user ? true : false,
    didLoginFail: false,
    user: user ? fromJS(user) : null,
    registerSuccess: false,
    isRegisterError: false,
    isRegisterNotValid: false
});

export default function AuthReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return state.set('loggedIn', fromJS(true)).set('user', fromJS(action.user)).set('didLoginFail', fromJS(false));
        case LOGIN_FAIL:
            return state.set('loggedIn', fromJS(false)).set('user', fromJS({})).set('didLoginFail', fromJS(true));
        case LOGOUT:
            return state.set('loggedIn', fromJS(false)).set('user', fromJS({}));
        case REGISTER_CHECK_EMAIL_SUCCESS:
            return state.setIn(['regisration', 'isEmailTaken'], fromJS(action.isTaken));
        case REGISTER_CHECK_USER_NAME_SUCCESS:
            return state.setIn(['regisration', 'isUserNameTaken'], fromJS(action.isTaken));
        case FETCH_PROFILE_SUCCESS:
            localStorage.setItem('user', JSON.stringify(action.user));
            return state.set('user', fromJS(action.user));
        case REGISTER_SUCCESS:
            return state.set('registerSuccess', fromJS(true)).set('isRegisterError', fromJS(false));
        case REGISTER_FAIL:
            return state.set('registerSuccess', fromJS(false)).set('isRegisterError', fromJS(true));
        case REGISTER_CHECK_VALIDATION: {
            if (action.isValid)
                return state.set('isRegisterNotValid', fromJS(false));
            return state.set('registerSuccess', fromJS(false)).set('isRegisterNotValid', fromJS(true));
        }
        default:
            return state
    }
}
