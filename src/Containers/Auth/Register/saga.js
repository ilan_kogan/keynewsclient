import {takeEvery, call, put} from 'redux-saga/effects';
import {toastr} from 'react-redux-toastr'
import {
    REGISTER,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
} from '../consts';
import {
    register,
    registerFail,
    registerSuccess,
    registerCheckEmail,
    registerCheckEmailFail,
    registerCheckEmailSuccess,
    registerCheckUserName,
    registerCheckUserNameFail,
    registerCheckUserNameSuccess
} from './actions';
import {
    registerApi,
} from '../../../Api/userService/register';

function *registerSaga({email, password}) {
    try {
        yield call(registerApi, email, password);
        yield put(registerSuccess());
    }
    catch (e) {
        yield put(registerFail());
        toastr.error('Signup Failed')
    }
}

// function *checkEmailSaga({email}){
//     try{
//         const response = yield call(checkEmailApi, email);
//         yield put(registerCheckEmailSuccess(response.isTaken));
//     }
//     catch (e){
//         yield put(registerCheckEmailFail());
//     }
// }
//
// function *checkUserNameSaga({userName}){
//     try{
//         const response = yield call(checkUserNameApi, userName);
//         yield put(registerCheckUserNameSuccess(response.isTaken));
//     }
//     catch (e){
//         yield put(registerCheckUserNameFail());
//     }
// }
//
export default function* defaultSaga() {
    yield takeEvery(REGISTER, registerSaga);
    // yield takeEvery(REGISTER_CHECK_EMAIL, checkEmailSaga);
    // yield takeEvery(REGISTER_CHECK_USER_NAME, checkUserNameSaga);
}
