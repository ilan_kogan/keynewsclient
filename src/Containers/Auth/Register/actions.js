import {REGISTER, REGISTER_SUCCESS, REGISTER_FAIL,REGISTER_CHECK_EMAIL,
    REGISTER_CHECK_USER_NAME,
    REGISTER_CHECK_EMAIL_SUCCESS,
    REGISTER_CHECK_EMAIL_FAIL,
    REGISTER_CHECK_USER_NAME_SUCCESS,
    REGISTER_CHECK_USER_NAME_FAIL,
    REGISTER_CHECK_VALIDATION,
    VERIFY_EMAIL,
    VERIFY_EMAIL_FAIL,
    VERIFY_EMAIL_SUCCESS
} from '../consts';

export function register(email, password){
    return{
        type: REGISTER,
        email,
        password,
    }
}

export function registerSuccess(){
    return{
        type: REGISTER_SUCCESS
    }
}

export function registerFail(){
    return{
        type: REGISTER_FAIL
    }
}

export function registerCheckEmail(email){
    return{
        type: REGISTER_CHECK_EMAIL,
        email
    }
}

export function registerCheckUserName(userName){
    return{
        type: REGISTER_CHECK_USER_NAME,
        userName
    }
}

export function registerCheckEmailSuccess(isTaken){
    return{
        type: REGISTER_CHECK_EMAIL_SUCCESS,
        isTaken
    }
}


export function registerCheckUserNameSuccess(isTaken){
    return{
        type: REGISTER_CHECK_USER_NAME_SUCCESS,
        isTaken
    }
}

export function registerCheckUserNameFail(){
    return{
        type: REGISTER_CHECK_USER_NAME_FAIL
    }
}

export function registerCheckEmailFail(){
    return{
        type: REGISTER_CHECK_EMAIL_FAIL
    }
}

export function registerCheckValidation(isValid){
    return{
        type: REGISTER_CHECK_VALIDATION,
        isValid
    }
}
