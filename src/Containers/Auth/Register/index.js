import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux'
import validator from 'validator';
import passwordValidator from 'password-validator'
import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import DEMO from "../../../Containers/TemplateContainer/constant";
import { register, registerCheckValidation } from "./actions";

const Register = ({ isRegisterError, registerSuccess, onSignUp, isDetailNotValid, registerValid }) => {
    const [validMessage, SetValidMessage] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const checkValid = (email, password) => {
        var schema = new passwordValidator();
        schema
            .is().min(8)                                    // Minimum length 8
            .is().max(100)                                  // Maximum length 100
            .has().uppercase()                              // Must have uppercase letters
            .has().lowercase()                              // Must have lowercase letters
            .has().digits()                                 // Must have digits
            .has().not().spaces()                           // Should not have spaces
        if (validator.isEmail(email) && schema.validate(password)) {
            registerValid(true)
            return onSignUp(email, password)
        }
        registerValid(false)
        !validator.isEmail(email) ? SetValidMessage('Please enter a valid Email') : SetValidMessage('Password must have: uppercase,lowercase,digits and at least 8 characters');
    }

    const handleEnter = (event) => {
        if (event.keyCode === 13)
            if (event.target.type === 'password')
                checkValid(email, password)
            else
                document.querySelector('#passwordInput').focus()
    }
    return (
        <Aux>
            <Breadcrumb />
            <div className="auth-wrapper">
                <div className="auth-content">
                    <div className="auth-bg">
                        <span className="r" />
                        <span className="r s" />
                        <span className="r s" />
                        <span className="r" />
                    </div>
                    <div className="card">
                        <div className="card-body text-center">
                            <div className="mb-4">
                                <i className="feather icon-user-plus auth-icon" />
                            </div>
                            <h3 className="mb-4">Sign up</h3>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email" onChange={handleEmailChange} onKeyUp={handleEnter} />
                            </div>
                            <div className="input-group mb-4">
                                <input type="password" className="form-control" id="passwordInput" placeholder="password" onChange={handlePasswordChange} onKeyUp={handleEnter} />
                            </div>
                            {/*<div className="form-group text-left">*/}
                            {/*    <div className="checkbox checkbox-fill d-inline">*/}
                            {/*        <input type="checkbox" name="checkbox-fill-2" id="checkbox-fill-2"/>*/}
                            {/*            <label htmlFor="checkbox-fill-2" className="cr">Send me the <a href={DEMO.BLANK_LINK}> Newsletter</a> weekly.</label>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {isRegisterError && <p className="mb-2 text-muted-red">Error at signup, your email might be taken </p>}
                            {isDetailNotValid && <p className="mb-2 text-muted-red"> {validMessage} </p>}
                            {registerSuccess && <p className="mb-2 text-muted-green">Your account has been created succefully please <NavLink to="/login">Login</NavLink></p>}
                            <button className="btn btn-primary shadow-2 mb-4" onClick={() => checkValid(email, password)}>Sign up</button>
                            <p className="mb-0 text-muted">Allready have an account? <NavLink to="/login">Login</NavLink></p>
                        </div>
                    </div>
                </div>
            </div>
        </Aux>
    );
}

const mapStateToProps = state => {
    return {
        isRegisterError: state.getIn(['authContainer', 'isRegisterError']),
        registerSuccess: state.getIn(['authContainer', 'registerSuccess']),
        isDetailNotValid: state.getIn(['authContainer', 'isRegisterNotValid'])
    }
};


const mapDispatchToProps = dispatch => {
    return {
        onSignUp: (email, password) => dispatch(register(email, password)),
        registerValid: (isValid) => dispatch(registerCheckValidation(isValid))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
