import {
    FETCH_PROFILE,
    FETCH_PROFILE_FAIL,
    FETCH_PROFILE_SUCCESS,
    EDIT_USER_NAME, 
    EDIT_FIRST_NAME,
    EDIT_LAST_NAME,
    EDIT_FIRST_NAME_FAIL,
    EDIT_LAST_NAME_FAIL,
    EDIT_USER_NAME_FAIL,
    EDIT_PASSWORD,
    EDIT_PASSWORD_FAIL,
    EDIT_PASSWORD_SUCCESS
} from '../consts';
import { USER_ID } from '../../../Redux/consts';

export function fetchProfile(){
    return{
        type: FETCH_PROFILE
    }
}

export function fetchProfileSuccess(user){
    return{
        type: FETCH_PROFILE_SUCCESS,
        user
    }
}

export function fetchProfileFail(){
    return{
        type: FETCH_PROFILE_FAIL
    }
}

export function editUserName(userName){
    return{
        type: EDIT_USER_NAME,
        userName
    }
} 

export function editFirstName(firstName){
    return{
        type: EDIT_FIRST_NAME,
        firstName
    }
} 

export function editLastName(lastName){
    return{
        type: EDIT_LAST_NAME,
        lastName
    }
} 

export function editUserNameFail(){
    return{
        type: EDIT_USER_NAME_FAIL
    }
}

export function editFirstNameFail(){
    return{
        type: EDIT_FIRST_NAME_FAIL
    }
}

export function editLastNameFail(){
    return{
        type: EDIT_LAST_NAME_FAIL
    }
}

export function editPassword(oldPassword, newPassword, userId){
    return{
        type: EDIT_PASSWORD,
        oldPassword, 
        newPassword,
        userId
    }
}

export function editPasswordSuccess(){
    return{
        type: EDIT_PASSWORD_SUCCESS
    }
}

export function editPasswordFail(){
    return{
        type: EDIT_PASSWORD_FAIL
    }
}