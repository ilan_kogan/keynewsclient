import React, { useState } from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import { logInRequest, logInRequestn } from './actions';
import { withRouter } from 'react-router-dom'

const Login = ({ onLogin, history, isLoggedIn, didLoginFail }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const goToRegister = () => {
        history.push('/register');
    };

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleEnter = (event) => {
        if (event.keyCode === 13)
            if (event.target.type === 'password')
                onLogin(email, password)
            else
                document.querySelector('#passwordInput').focus()
    }
    return (
        isLoggedIn ? <Redirect to={{ pathName: '/' }} /> :
            <Aux>
                <Breadcrumb />
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r" />
                            <span className="r s" />
                            <span className="r s" />
                            <span className="r" />
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-unlock auth-icon" />
                                </div>
                                <h3 className="mb-4">Login</h3>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" onChange={handleEmailChange} onKeyUp={handleEnter} />
                                </div>
                                <div className="input-group mb-4">
                                    <input type="password" className="form-control" id="passwordInput" placeholder="password" onChange={handlePasswordChange} onKeyUp={handleEnter} />
                                </div>
                                {didLoginFail && <p className="mb-2 text-muted-red">Check your email or password </p>}
                                <button className="btn btn-primary shadow-2 mb-4" onClick={() => onLogin(email, password)}>Login</button>
                                {/*<p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1">Reset</NavLink></p>*/}
                                <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/register">Signup</NavLink></p>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
    );
};

const mapStateToProps = state => {
    return {
        isLoggedIn: state.getIn(['authContainer', 'loggedIn']),
        didLoginFail: state.getIn(['authContainer', 'didLoginFail'])

    }
};

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (email, password) => dispatch(logInRequest(email, password)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);
