import {
    LOGIN_FAIL,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGOUT,
    FORGOT_CHANGE_PASSWORD,
    FORGOT_PASSWORD
} from '../consts';

export function logInRequest(email, password) {
    return {
        type: LOGIN_REQUEST,
        email,
        password
    }
}

export function logInSuccess(user) {
    return{
        type: LOGIN_SUCCESS,
        user
    }
}

export function logInFail() {
    return{
        type: LOGIN_FAIL
    }
}

export function logOut() {
    return{
        type: LOGOUT
    }
}

export function forgotPassword(email){
    return{
        type: FORGOT_PASSWORD,
        email
    }
}

export function forgotChangePassword(token, password, userId){
    return{
        type: FORGOT_CHANGE_PASSWORD,
        token,
        password,
        userId
    }
}

