import { takeEvery, call, put } from 'redux-saga/effects';
import {toastr} from 'react-redux-toastr'
import {
    LOGIN_FAIL,
    LOGIN_REQUEST,
    LOGOUT,
    FORGOT_CHANGE_PASSWORD,
    FORGOT_PASSWORD
} from '../consts';
import {
    logInSuccess,
    logInFail,
} from './actions';
import {
    loginApi,
    forgotPasswordApi,
    forgotPasswordChangeApi
} from '../../../Api/userService/login';

// function *invitationLogin({invitationId, token}){
//     try{
//         const authenticationData = yield call(invitationLoginApi, invitationId, token);
//         localStorage.setItem('user', JSON.stringify(authenticationData.user));
//         yield put(invitationLoginFail);
//     }
//     catch(error){
//         yield put(invitationLoginFail);
//     }
// }
//
// function *guestLogin({fingerPrint}){
//     try{
//         const authenticationData = yield call(guestLoginApi, fingerPrint);
//         localStorage.setItem('user', JSON.stringify(authenticationData.user));
//         yield put(guestLoginSuccess(authenticationData.user));
//     }
//     catch (e) {
//         yield put(guestLoginFail());
//     }
// }
//
function* login({ email, password }) {
    try {
        const authenticationData = yield call(loginApi, email, password);
        localStorage.setItem('user', JSON.stringify(authenticationData.user));
        yield put(logInSuccess(authenticationData.user));
    }
    catch (e) {
        toastr.error('Login Failed')
        yield put(logInFail());
    }
}
//
// function *googleLogin({accessToken, userId}){
//     try{
//         const authenticationData = yield call(googleLoginApi, accessToken, userId);
//         localStorage.setItem('user', JSON.stringify(authenticationData.user));
//         yield put(logInSuccess(authenticationData.user));
//     }
//     catch (e){
//         yield put(logInFail());
//     }
// }
//
// function *facebookLogin({accessToken, userId}){
//     try{
//         const authenticationData = yield call(facebookLoginApi, accessToken, userId);
//         localStorage.setItem('user', JSON.stringify(authenticationData.user));
//         yield put(logInSuccess(authenticationData.user));
//     }
//     catch (e){
//         yield put(logInFail());
//     }
// }
//
function* logout() {
    localStorage.removeItem('user');
}
//
// function *forgotPassword({email}){
//     try{
//         yield call(forgotPasswordApi, email);
//     }
//     catch{
//         console.log('Failed trying to send recover password email');
//     }
// }
//
// function *forgotChangePassword({token, password, userId}){
//     try{
//         yield call(forgotPasswordChangeApi, token, password, userId);
//     }
//     catch{
//         console.log('failed change password');
//     }
// }

export default function* defaultSaga() {
    // yield takeEvery(GUEST_LOGIN_REQUEST, guestLogin);
    yield takeEvery(LOGIN_REQUEST, login);
    // yield takeEvery(LOGIN_FAIL, logout);
    yield takeEvery(LOGOUT, logout);
    // yield takeEvery(GOOGLE_LOGIN_REQUEST, googleLogin);
    // yield takeEvery(FACEBOOK_LOGIN_REQUEST, facebookLogin);
    // yield takeEvery(FORGOT_PASSWORD, forgotPassword);
    // yield takeEvery(FORGOT_CHANGE_PASSWORD, forgotChangePassword);
}
