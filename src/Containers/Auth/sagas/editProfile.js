// import {takeEvery, call, put} from 'redux-saga/effects';
// import {
//     FETCH_PROFILE,
//     EDIT_USER_NAME,
//     EDIT_LAST_NAME,
//     EDIT_FIRST_NAME,
//     EDIT_PASSWORD
// } from '../consts';
// import {
//     fetchProfile,
//     fetchProfileSuccess,
//     fetchProfileFail,
//     editUserNameFail,
//     editFirstName,
//     editLastName,
//     editFirstNameFail,
//     editLastNameFail,
//     editPasswordFail,
//     editPasswordSuccess
// } from '../actions/editProfile';
// import {
//     changeUserNameApi,
//     changeFirstNameApi,
//     changeLastNameApi,
//     getProfileApi,
//     changePasswordApi
// } from '../../../api/frontendServer/users/editProfile';
//
// function *fetchProfileSaga({userId}){
//     try{
//         const user = yield call(getProfileApi, userId);
//         yield put(fetchProfileSuccess(user));
//     }
//     catch(e){
//         yield put(fetchProfileFail());
//     }
// }
//
// function *editUserNameSaga({userName, userId}) {
//     try {
//         yield call(changeUserNameApi, userName, userId);
//         yield put(fetchProfile());
//     }
//     catch (e) {
//         yield put(editUserNameFail());
//     }
// }
//
// function *editFirstNameSaga({firstName, userId}) {
//     try {
//         yield call(changeFirstNameApi, firstName, userId);
//         yield put(fetchProfile());
//     }
//     catch (e) {
//         yield put(editFirstNameFail());
//     }
// }
//
// function *editLastNameSaga({lastName, userId}) {
//     try {
//         yield call(changeLastNameApi, lastName, userId);
//         yield put(fetchProfile());
//     }
//     catch (e) {
//         yield put(editLastNameFail());
//     }
// }
//
// function *editPasswordSaga({oldPassword, newPassword, userId}){
//     try{
//         yield call(changePasswordApi, oldPassword, newPassword, userId);
//         yield put(editPasswordFail());
//     }
//     catch(e){
//         yield put(editPasswordSuccess());
//     }
// }
//
// export default function* defaultSaga() {
//     yield takeEvery(EDIT_USER_NAME, editUserNameSaga);
//     yield takeEvery(FETCH_PROFILE, fetchProfileSaga);
//     yield takeEvery(EDIT_FIRST_NAME, editFirstNameSaga);
//     yield takeEvery(EDIT_LAST_NAME, editLastNameSaga);
//     yield takeEvery(EDIT_PASSWORD, editPasswordSaga);
// }
