import {
    FETCH_COUNT_LINKS_REQUEST,
    FETCH_COUNT_LINKS_SUCCESS,
    FETCH_COUNT_LINKS_FAIL,
    FETCH_ALL_KEYWORDS_SUCCESS,
    FETCH_ALL_KEYWORDS_REQUEST,
    FETCH_ALL_KEYWORDS_FAIL,
    ADD_KEYWORD_MODE_TRUE,
    ADD_KEYWORD_MODE_fALSE,
    ADD_KEYWORD_SUCCESS,
    DELETE_MODE_ON,
    DELETE_MODE_OFF,
    DELETE_KEYWORD_SUCCESS,
    DELETE_KEYWORD_FAIL,
    DELETE_All_KEYWORDS_FAIL,
    DELETE_All_KEYWORDS_SUCCESS,
    FETCH_AUTOSENDING,
    FETCH_AUTOSENDING_SUCCESS,
    FETCH_AUTOSENDING_FAIL,
    FETCH_ALL_DATA_SUCCESS,
    FETCH_ALL_DATA_FAIL,
    FETCH_ALL_DATA,
    LOGOUT
} from "./consts";

import { fromJS } from "immutable";

const initialState = fromJS({
    numOfLinksGot: 0,
    isLinksCountLoading: false,
    errorFetchingLinksCount: false,
    autoSending: 0,
    isAutoSendingLodaing: false,
    errorFetchingAutoSending: false,
    keywords: [],
    isKeywordsLoading: true,
    errorFetchingKeywords: false,
    addKey: false,
    deleteMode: false
});

export default function ArticlesReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_ALL_DATA:
            return state.set('isAutoSendingLodaing', fromJS(true)).set('isLinksCountLoading', fromJS(true)).set('isKeywordsLoading', fromJS(true));
        case FETCH_ALL_DATA_SUCCESS:
            return state.set('autoSending', fromJS(action.allData.autoSending)).set('isAutoSendingLodaing', fromJS(false)).set('errorFetchingAutoSending', fromJS(false))
                .set('numOfLinksGot', fromJS(action.allData.linksCount)).set('isLinksCountLoading', fromJS(false)).set('errorFetchingLinksCount', fromJS(false))
                .set('errorFetchingKeywords', fromJS(false)).set('keywords', fromJS(action.allData.keywords)).set('isKeywordsLoading', fromJS(false));
        case FETCH_ALL_DATA_FAIL:
            return state.set('isAutoSendingLodaing', fromJS(false)).set('autoSending', fromJS(0)).set('errorFetchingAutoSending', fromJS(true))
                .set('isLinksCountLoading', fromJS(false)).set('numOfLinksGot', fromJS(0)).set('errorFetchingLinksCount', fromJS(true))
                .set('errorFetchingKeywords', fromJS(true)).set('keywords', fromJS([])).set('isKeywordsLoading', fromJS(false));
        case FETCH_AUTOSENDING:
            return state.set('isAutoSendingLodaing', fromJS(true));
        case FETCH_AUTOSENDING_SUCCESS:
            return state.set('autoSending', fromJS(action.autoSending)).set('isAutoSendingLodaing', fromJS(false)).set('errorFetchingAutoSending', fromJS(false));
        case FETCH_AUTOSENDING_FAIL:
            return state.set('isAutoSendingLodaing', fromJS(false)).set('autoSending', fromJS(0)).set('errorFetchingAutoSending', fromJS(true));
        case FETCH_COUNT_LINKS_REQUEST:
            return state.set('isLinksCountLoading', fromJS(true));
        case FETCH_COUNT_LINKS_SUCCESS:
            return state.set('numOfLinksGot', fromJS(action.linksCount)).set('isLinksCountLoading', fromJS(false)).set('errorFetchingLinksCount', fromJS(false));
        case FETCH_COUNT_LINKS_FAIL:
            return state.set('isLinksCountLoading', fromJS(false)).set('numOfLinksGot', fromJS(0)).set('errorFetchingLinksCount', fromJS(true));
        case FETCH_ALL_KEYWORDS_REQUEST:
            return state.set('isKeywordsLoading', fromJS(true));
        case FETCH_ALL_KEYWORDS_FAIL:
            return state.set('errorFetchingKeywords', fromJS(true)).set('keywords', fromJS([])).set('isKeywordsLoading', fromJS(false));
        case FETCH_ALL_KEYWORDS_SUCCESS:
            return state.set('errorFetchingKeywords', fromJS(false)).set('keywords', fromJS(action.keywords)).set('isKeywordsLoading', fromJS(false));
        case ADD_KEYWORD_MODE_TRUE:
            return state.set('addKey', fromJS(true));
        case ADD_KEYWORD_MODE_fALSE:
            return state.set('addKey', fromJS(false));
        case ADD_KEYWORD_SUCCESS:
            return state.set('addKey', fromJS(false));
        case DELETE_MODE_ON:
            return state.set('deleteMode', fromJS(true));
        case DELETE_MODE_OFF:
            return state.set('deleteMode', fromJS(false));
        case DELETE_KEYWORD_SUCCESS:
            return state.set('deleteMode', fromJS(true));
        case DELETE_KEYWORD_FAIL:
            return state.set('deleteMode', fromJS(false));
        case DELETE_All_KEYWORDS_SUCCESS:
            return state.set('deleteMode', fromJS(true));
        case DELETE_All_KEYWORDS_FAIL:
            return state.set('deleteMode', fromJS(false));
        case LOGOUT:
            return initialState;
        default:
            return state
    }
}
