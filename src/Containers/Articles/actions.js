import {
    FETCH_COUNT_LINKS_FAIL,
    FETCH_COUNT_LINKS_REQUEST,
    FETCH_COUNT_LINKS_SUCCESS,
    FETCH_ALL_KEYWORDS_FAIL,
    FETCH_ALL_KEYWORDS_REQUEST,
    FETCH_ALL_KEYWORDS_SUCCESS,
    EDIT_KEYWORD_REQUEST,
    EDIT_KEYWORD_REQUEST_FAIL,
    EDIT_KEYWORD_REQUEST_SUCCESS,
    ADD_KEYWORD_FAIL,
    ADD_KEYWORD_REQUEST,
    ADD_KEYWORD_SUCCESS,
    ADD_KEYWORD_MODE_fALSE,
    ADD_KEYWORD_MODE_TRUE,
    DELETE_MODE_OFF,
    DELETE_MODE_ON,
    DELETE_KEYWORD_FAIL,
    DELETE_KEYWORD_REQUEST,
    DELETE_KEYWORD_SUCCESS,
    FETCH_AUTOSENDING,
    FETCH_AUTOSENDING_SUCCESS,
    FETCH_AUTOSENDING_FAIL,
    CHANGE_AUTOSENDING_REQUEST,
    CHANGE_AUTOSENDING_REQUEST_FAIL,
    CHANGE_AUTOSENDING_REQUEST_SUCCESS,
    FETCH_ALL_DATA,
    FETCH_ALL_DATA_FAIL,
    FETCH_ALL_DATA_SUCCESS,
    DELETE_All_KEYWORDS_REQUEST,
    DELETE_All_KEYWORDS_SUCCESS,
    DELETE_All_KEYWORDS_FAIL
} from './consts';

export function deleteKeyword(keywordId) {
    return {
        type: DELETE_KEYWORD_REQUEST,
        keywordId
    }
}

export function deleteKeywordSuccess() {
    return {
        type: DELETE_KEYWORD_SUCCESS,
    }
}

export function deleteKeywordFail() {
    return {
        type: DELETE_KEYWORD_FAIL,
    }
}

export function deleteAllKeywords(keywordId) {
    return {
        type: DELETE_All_KEYWORDS_REQUEST,
        keywordId
    }
}

export function deleteAllKeywordsSuccess() {
    return {
        type: DELETE_All_KEYWORDS_SUCCESS,
    }
}

export function deleteAllKeywordsFail() {
    return {
        type: DELETE_All_KEYWORDS_FAIL,
    }
}

export function deleteModeOn() {
    return {
        type: DELETE_MODE_ON,
    }
}

export function deleteModeOff() {
    return {
        type: DELETE_MODE_OFF,
    }
}
//fetch autoSending keywords and links count
export function fetchAllData() {
    return {
        type: FETCH_ALL_DATA
    }
}

export function fetchAllDataSuccess(allData) {
    return{
        type: FETCH_ALL_DATA_SUCCESS,
        allData
    }
}

export function fetchAllDataFail() {
    return{
        type: FETCH_ALL_DATA_FAIL
    }
}

export function fetchKeywords() {
    return {
        type: FETCH_ALL_KEYWORDS_REQUEST,
    }
}

export function fetchKeywordsSuccess(keywords) {
    return {
        type: FETCH_ALL_KEYWORDS_SUCCESS,
        keywords
    }
}

export function fetchKeywordsFaill() {
    return {
        type: FETCH_ALL_KEYWORDS_FAIL,
    }
}

export function fetchCountLinks() {
    return {
        type: FETCH_COUNT_LINKS_REQUEST
    }
}

export function fetchCountLinksSuccess(linksCount) {
    return{
        type: FETCH_COUNT_LINKS_SUCCESS,
        linksCount
    }
}

export function fetchCountLinksFail() {
    return{
        type: FETCH_COUNT_LINKS_FAIL
    }
}

export function fetchAutoSending() {
    return {
        type: FETCH_AUTOSENDING
    }
}

export function fetchAutoSendingSuccess(autoSending) {
    return{
        type: FETCH_AUTOSENDING_SUCCESS,
        autoSending
    }
}

export function fetchAutoSendingFail() {
    return{
        type: FETCH_AUTOSENDING_FAIL
    }
}

export function changeAutoSending(autoSendingStatus) {
    return{
        type: CHANGE_AUTOSENDING_REQUEST,
        autoSendingStatus
    }
}

export function changeAutoSendingSuccess() {
    return{
        type: CHANGE_AUTOSENDING_REQUEST_SUCCESS
    }
}

export function changeAutoSendingFail() {
    return{
        type: CHANGE_AUTOSENDING_REQUEST_FAIL
    }
}

export function editKeyword(keywordId, value, language) {
    return{
        type: EDIT_KEYWORD_REQUEST,
        keywordId,
        value,
        language
    }
}

export function editKeywordSuccess() {
    return{
        type: EDIT_KEYWORD_REQUEST_SUCCESS
    }
}

export function editKeywordFail() {
    return{
        type: EDIT_KEYWORD_REQUEST_FAIL
    }
}

export function addKeyword(value, language) {
    return{
        type: ADD_KEYWORD_REQUEST,
        value,
        language
    }
}

export function addKeywordFail() {
    return{
        type: ADD_KEYWORD_FAIL
    }
}

export function addKeywordSuccess() {
    return{
        type: ADD_KEYWORD_SUCCESS
    }
}

export function addKeywordModeOn() {
    return{
        type: ADD_KEYWORD_MODE_TRUE
    }
}

export function addKeywordModeOff() {
    return{
        type: ADD_KEYWORD_MODE_fALSE
    }
}
