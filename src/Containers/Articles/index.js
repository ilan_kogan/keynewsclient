import React, { useEffect } from 'react';
import { Row, Col, Card, Table, Tabs, Tab } from 'react-bootstrap';
import { connect } from 'react-redux';
import Aux from "../../hoc/_Aux";
import { addKeywordModeOn, deleteModeOn, deleteModeOff, changeAutoSending, fetchAllData, deleteAllKeywords as deleteAllKeywordsAction } from './actions';
import Keyword from "../../Components/Keyword";
import AddKeyword from '../../Components/NewKeyword';
import deleteIcon from '../../assets/images/keywords/remove.svg';
import editIcon from '../../assets/images/keywords/edit.svg';
import styles from './styles.module.css';


const Dashboard = ({ numOfLinksGot, isLinksCountLoading, errorLinksCount, keywords,
    isKeywordsLoading, errorFetchingKeywords, addKeyMode, addKeyModeOn, enterDeleteMode,
    exitDeleteMode, inDeleteKeywordMode, autoSending, isAutoSendingLoading, errorAutoSending, editAutoSending, fetchDataAll, deleteAllKeywords }) => {

    useEffect(() => {
        fetchDataAll();
    }, []);

    const handleClickAutoSend = () => {
        if (autoSending)
            return editAutoSending(0)
        return editAutoSending(1)
    }
    return (
        <Aux >
            <Row>
                <Col md={6} xl={4}>
                    <Card>
                        <Card.Body>
                            <h6 className='mb-4'>Settings</h6>
                            <div className="row d-flex align-items-center">
                                <div className="col-9">
                                    <div class="form-group"><div class="custom-control custom-checkbox">
                                        {isAutoSendingLoading ? `Loading...` : errorAutoSending ? `Error getting settings, try to clear site data` :
                                            <span>
                                                <input type="checkbox" id="checkbox1" class="custom-control-input" checked={autoSending} onClick={handleClickAutoSend} />
                                                <label title="" type="checkbox" for="checkbox1" class="custom-control-label" style={{ color: "black" }}>
                                                    Auto emails sending
                                                </label>
                                            </span>}
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col md={6} xl={4}>
                    <Card>
                        <Card.Body>
                            <h6 className='mb-4'>Articles received to your email</h6>
                            <div className="row d-flex align-items-center">
                                <div className="col-9">
                                    <h3 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-arrow-up text-c-green f-30 m-r-5" />
                                        {isLinksCountLoading ? `Loading...` : errorLinksCount ? `Failed to load, try to clear site data` : numOfLinksGot}
                                    </h3>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col md={6} xl={8}>
                    <Card className='Recent-Users'>
                        <Card.Header>
                            <Card.Title as='h5'>Your keywords
                                </Card.Title>
                            <span className={styles.deleteWrapper}>
                                {inDeleteKeywordMode ? <img className={styles.delete} alt={'editKeywords'} style={{ width: "25.66px", height: "26px" }} src={editIcon} onClick={exitDeleteMode} /> :
                                    <img className={styles.delete} alt={'deleteKeywords'} src={deleteIcon} style={{ width: "25.66px", height: "26px" }} onClick={() => { if (keywords.toJS().length != 0) enterDeleteMode() }} />
                                }
                                <a className="label theme-bg-green text-white f-12" onClick={addKeyModeOn}>Add keyword</a>
                                <a className="label theme-bg-green text-white f-12" onClick={() => { if (keywords.toJS().length != 0) deleteAllKeywords() }}>Delete all keywords</a>
                            </span>
                        </Card.Header>
                        <Card.Body className='px-0 py-2'>
                            <div >
                                <div style={{ display: 'flex', flexDirection: 'column' }}>
                                    {isKeywordsLoading ?
                                        <tr className="unread">
                                            <td>
                                                <h6 className="mb-1">Loading...</h6>
                                            </td>
                                        </tr>
                                        : errorFetchingKeywords ?
                                            <td>
                                                <h6 className="mb-1">Error getting keywords, please try to clear site data</h6>
                                            </td> : addKeyMode ?
                                                <React.Fragment>
                                                    <AddKeyword />
                                                    {keywords.toJS().map((keyword, index) => <Keyword key={index} id={keyword.id} value={keyword.value} language={keyword.language} />)}
                                                </React.Fragment>
                                                : keywords.toJS().length == 0 && exitDeleteMode() ? <span></span> :
                                                    keywords.toJS().map((keyword, index) => <Keyword key={index} id={keyword.id} value={keyword.value} language={keyword.language} />)
                                    }
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Aux >
    );
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDataAll: () => dispatch(fetchAllData()),
        addKeyModeOn: () => dispatch(addKeywordModeOn()),
        enterDeleteMode: () => dispatch(deleteModeOn()),
        exitDeleteMode: () => dispatch(deleteModeOff()),
        editAutoSending: (value) => dispatch(changeAutoSending(value)),
        deleteAllKeywords: () => dispatch(deleteAllKeywordsAction())
    }
};

const mapStateToProps = state => {
    return {
        numOfLinksGot: state.getIn(['articlesContainer', 'numOfLinksGot']),
        isLinksCountLoading: state.getIn(['articlesContainer', 'isLinkCountLoading']),
        errorLinksCount: state.getIn(['articlesContainer', 'errorFetchingLinksCount']),
        autoSending: state.getIn(['articlesContainer', 'autoSending']),
        isAutoSendingLoading: state.getIn(['articlesContainer', 'isAutoSendingLodaing']),
        errorAutoSending: state.getIn(['articlesContainer', 'errorFetchingAutoSending']),
        keywords: state.getIn(['articlesContainer', 'keywords']),
        isKeywordsLoading: state.getIn(['articlesContainer', 'isKeywordsLoading']),
        errorFetchingKeywords: state.getIn(['articlesContainer', 'errorFetchingKeywords']),
        addKeyMode: state.getIn(['articlesContainer', 'addKey']),
        inDeleteKeywordMode: state.getIn(['articlesContainer', 'deleteMode'])
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
