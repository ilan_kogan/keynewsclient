import { toastr } from 'react-redux-toastr'
import { takeEvery, call, put } from 'redux-saga/effects';
import {
    FETCH_COUNT_LINKS_REQUEST,
    FETCH_ALL_KEYWORDS_REQUEST,
    EDIT_KEYWORD_REQUEST,
    ADD_KEYWORD_REQUEST,
    DELETE_KEYWORD_REQUEST,
    FETCH_AUTOSENDING,
    CHANGE_AUTOSENDING_REQUEST,
    FETCH_ALL_DATA,
    DELETE_All_KEYWORDS_REQUEST
} from './consts';
import {
    fetchKeywords,
    fetchCountLinksFail,
    fetchCountLinksSuccess,
    fetchKeywordsFaill,
    fetchKeywordsSuccess,
    editKeywordSuccess,
    editKeywordFail,
    addKeywordFail,
    addKeywordSuccess,
    deleteKeywordSuccess,
    deleteKeywordFail,
    deleteAllKeywordsSuccess,
    deleteAllKeywordsFail,
    fetchAutoSendingSuccess,
    fetchAutoSendingFail,
    changeAutoSendingFail,
    changeAutoSendingSuccess,
    fetchAutoSending,
    fetchAllDataFail,
    fetchAllDataSuccess
} from './actions';
import {
    fetchLinksCount,
    fetchAllKeywords,
    editKeyWord as editKeywordApi,
    addKeyword as addKeywordApi,
    deleteKeywordApi,
    deleteAllKeywordsApi,
    fetchAutoSending as getAutoSending,
    changeAutoSending as editAutoSending,
    fetchAllData
} from '../../Api/userService/articles';


function* getLinksCount() {
    try {
        const linksCount = yield call(fetchLinksCount);
        yield put(fetchCountLinksSuccess(linksCount));
    }
    catch (error) {
        yield put(fetchCountLinksFail());
    }
}

function* getKeywords() {
    try {
        const keywords = yield call(fetchAllKeywords);
        yield put(fetchKeywordsSuccess(keywords));
    }
    catch (error) {
        yield put(fetchKeywordsFaill());
    }
}

function* editKeywords({ keywordId, value, language }) {
    try {
        yield call(editKeywordApi, keywordId, value, language);
        yield put(editKeywordSuccess());
        yield put(fetchKeywords());
        toastr.success('Edit', 'keyword has been changed');
    }
    catch (e) {
        toastr.error('Server Error', "Can't edit keyword")
        yield put(editKeywordFail());
    }
}

function* addKeyword({ value, language }) {
    try {
        yield call(addKeywordApi, value, language);
        yield put(addKeywordSuccess());
        yield put(fetchKeywords());
        toastr.success('Add', 'added keyword successfully');
    }
    catch (e) {
        yield put(addKeywordFail());
        toastr.error('Server Error', "Can't add keyword")
    }
}
function* deleteKeyword({ keywordId }) {
    try {
        yield call(deleteKeywordApi, keywordId);
        yield put(deleteKeywordSuccess());
        yield put(fetchKeywords());
        toastr.success('Delete', 'deleted keyword successfully');
    }
    catch (e) {
        yield put(deleteKeywordFail());
        toastr.error('Server Error', "Can't delete keyword")
    }
}

function* deleteAllKeywords({ keywordId }) {
    try {
        yield call(deleteAllKeywordsApi);
        yield put(deleteAllKeywordsSuccess());
        yield put(fetchKeywords());
        toastr.success('Delete', 'deleted all keywords successfully');
    }
    catch (e) {
        yield put(deleteAllKeywordsFail());
        toastr.error('Server Error', "Can't delete keywords")
    }
}

function* getAutoSendingStaus() {
    try {
        const autoSending = yield call(getAutoSending);
        yield put(fetchAutoSendingSuccess(autoSending));
    }
    catch (error) {
        yield put(fetchAutoSendingFail());
    }
}

function* changeAutoSending({autoSendingStatus}) {
    try {
        yield call(editAutoSending, autoSendingStatus);
        yield put(changeAutoSendingSuccess());
        yield put(fetchAutoSending());
    }
    catch (e) {
        toastr.error('Server Error', "Can't change settings")
        yield put(changeAutoSendingFail());
    }
}

function* getAlldata() {
    try {
        const allData = yield call(fetchAllData);
        yield put(fetchAllDataSuccess(allData));
    }
    catch (error) {
        yield put(fetchAllDataFail());
    }
}

export default function* defaultSaga() {
    yield takeEvery(FETCH_COUNT_LINKS_REQUEST, getLinksCount);
    yield takeEvery(FETCH_ALL_KEYWORDS_REQUEST, getKeywords);
    yield takeEvery(EDIT_KEYWORD_REQUEST, editKeywords);
    yield takeEvery(ADD_KEYWORD_REQUEST, addKeyword);
    yield takeEvery(DELETE_KEYWORD_REQUEST, deleteKeyword);
    yield takeEvery(DELETE_All_KEYWORDS_REQUEST, deleteAllKeywords);
    yield takeEvery(FETCH_AUTOSENDING, getAutoSendingStaus);
    yield takeEvery(CHANGE_AUTOSENDING_REQUEST, changeAutoSending);
    yield takeEvery(FETCH_ALL_DATA, getAlldata);
}
