export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                },
                {
                    id: 'myNews',
                    title: 'My news',
                    type: 'item',
                    url: '/news',
                    icon: 'feather icon-align-left',
                }
            ]
        },

    ]
}
