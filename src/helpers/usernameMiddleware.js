import {userIdSelector, loggedInSelector} from '../Containers/Auth/selectors';

export const usernameMiddleware = state => next => action => {
  const loggedIn = loggedInSelector(state.getState());
  if(!loggedIn) return next(action);
  else{
    const userId = userIdSelector(state.getState());
    action.userId = userId;
  }
  next(action)
};
