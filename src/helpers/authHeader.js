export default function authHeader() {
    // return authorization header with jwt token
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
        return { Authorization: 'JWT ' + user.token};
    } else {
        return {};
    }
}