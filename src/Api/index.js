import axios from "axios";
import config from '../config/config'
import authHeader from "../helpers/authHeader";

const userService = config.userService.host;

export default class Request {
    static get(path){
        return axios.get(userService + path, {headers: authHeader()}).then(Request.getResData)
    }

    static post(path, body){
        return axios.post(userService + path, body,{headers: authHeader()}).then(Request.getResData)
    }

    static put(path, body){
        return axios.put(userService + path, body, {headers: authHeader()}).then(Request.getResData)
    }

    static delete(path, body){
        return axios.delete(userService + path, {headers: authHeader(),data:body}).then(Request.getResData)
    }

    static getResData(res){
        return res.data;
    }
}
