import Request from "..";
import config from '../../config/config';

const {userService} = config;
const {register} = userService;

export  async function registerApi(email, password){
    return await Request.post(register,{email, password});
}

