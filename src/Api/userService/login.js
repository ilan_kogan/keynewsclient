import Request from "..";
import config from '../../config/config';

const {userService} = config;
const {login, forgotPassword, forgotChangePassword} = userService;

export  async function loginApi(email, password){
    return await Request.post(login,{email, password});
}

export async function forgotPasswordApi(email){
    await Request.post(forgotPassword, {email});
}

export async function forgotPasswordChangeApi(token, password, userId){
    await Request.post(forgotChangePassword, {token, password, userId});
}
