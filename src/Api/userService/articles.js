import Request from "..";
import config from '../../config/config';

const { userService } = config;
const { linksCount, getKeywords, editKeyword, createKeyword, deleteKeyword, autoSending, allData, deleteAllKeywords, articles } = userService;

export async function fetchAllData() {
    const result = await Request.get(allData);
    return result;
}

export async function fetchLinksCount() {
    const result = await Request.get(linksCount);
    return result.linksCount;
}

export async function fetchAllKeywords() {
    const result = await Request.get(getKeywords);
    return result.keywords;
}

export async function fetchAutoSending() {
    const result = await Request.get(autoSending);
    return result.autoSending;
}

export async function changeAutoSending(value) {
    await Request.post(autoSending, { value });
}

export async function editKeyWord(keywordId, value, language) {
    await Request.post(editKeyword, { keywordId, value, language });
}

export async function addKeyword(value, language) {
    await Request.post(createKeyword, { value, language });
}

export async function deleteKeywordApi(keywordId) {
    await Request.delete(deleteKeyword, { keywordId });
}

export async function deleteAllKeywordsApi() {
    await Request.delete(deleteAllKeywords);
}

export async function fetchArticles(limit, offset, id) {
    const result = await Request.get(`${articles}?limit=${limit}&offset=${offset}`,{ id });
    return result.articles;
}