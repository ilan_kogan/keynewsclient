**KeyNews**

This project was created as a poc.
Started by me and now my friend working on it in order to learn web development, i am no longer responsible for the code.

This project goal is to send relevant articles to the user based on personal
keywords he choose.

Currently you can use english and hebrew keywords.

This project is very similar to "google alerts".

Please keep in mind that this project isnt complete, for example the authentication part lacking some functionality like forgot password.

KeyNews consists of a client, "user service" which is the backend , and thrid service "MrKnowAll" that responsible for sending relevant articles.

I used here the free version of news api which you can find here : https://newsapi.org/

Here is a link to a demo that is up and running : http://18.223.120.1:3001

*articles will be send every 15 minutes from the time MrKnowAll started to run
